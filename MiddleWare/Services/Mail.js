import config from '../../Config'
import sgMail from '@sendgrid/mail'

let MailService = module.exports = {};
sgMail.setApiKey(config.SendGridKey);

MailService.SendPromoCode = function (username, email, promoCode) {
    const msg = {
        to: email,
        from: 'swvl@gmail.com',
        subject: '[SWVL] Promo Code',
        html:
            `
        <div> 
            <h1> Welcome ${username} to SWVL</h1>
            <h2>Your Promo Code is : <p> <strong>${promoCode}</strong> </p></h2>
		</div>
                `
    };
    return new Promise(function (resolve, reject) {
        sgMail.send(msg);
        resolve(true);
    })
}

MailService.SendMessage = function (username, email, message) {
    const msg = {
        to: email,
        from: 'swvl@gmail.com',
        subject: '[SWVL]',
        html:
            `
        <div> 
            <h1> Welcome ${username} to SWVL</h1>
            <p>${message}</p>
		</div>
                `
    };
    return new Promise(function (resolve, reject) {
        sgMail.send(msg);
        resolve(true);
    })
}
