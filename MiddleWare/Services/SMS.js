import Nexmo from 'nexmo'
import config from '../../Config'
let SendSMSService = module.exports = {}

const nexmo = new Nexmo({
    apiKey: config.Nexmo.ApiKey,
    apiSecret: config.Nexmo.ApiSecret,
})

SendSMSService.SendPromoCode = function (userNumber, promoCodeMessage, promoCode) {
    const from = 'SWVL'
    const to = `2${userNumber}`
    const text = `${promoCodeMessage} : ${promoCode}`
    nexmo.message.sendSms(from, to, text)
}

SendSMSService.SMS = function (userNumber, textMessage) {
    console.log(textMessage)
    const opts = {
        "type": "unicode"
    }
    const from = 'SWVL'
    const to = `2${userNumber}`
    const text = `Message From SWVL : ${textMessage}`

    nexmo.message.sendSms(from, to, text, opts, (err, responseData) => {
        if (err) {
            console.log(err);
        } else {
            if (responseData.messages[0]['status'] === "0") {
                console.log("Message sent successfully.");
            } else {
                console.log(`Message failed with error: ${responseData.messages[0]['error-text']}`);
            }
        }
    })
}
