import config from '../../Config'
import OneSignal from 'onesignal-node'

module.exports = async function (message, deviceToken) {
    var myClient = new OneSignal.Client({
        userAuthKey: config.OneSignal.UserAuthKey,
        app: { appAuthKey: config.OneSignal.AppAuthKey, appId: config.OneSignal.AppId }
    })

    var firstNotification = new OneSignal.Notification({
        contents: {
            en: message
        },
        include_player_ids: [deviceToken]
    })

    const res = await myClient.sendNotification(firstNotification)
    return res
}
