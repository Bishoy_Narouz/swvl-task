import errors from '../../Errors/index'
import validator from 'validator'

module.exports = function array({ array, fieldName } = {}) {
    if (!array || !Array.isArray(array)) {
        throw new errors.Missing({ fieldName })
    } else {
        for (let id of array) {
            if ((typeof id !== 'string') || !validator.isMongoId(id)) {
                throw new errors.Invalid({ message: `${fieldName} is not valid` })
            }
        }
    }
    return array
}
