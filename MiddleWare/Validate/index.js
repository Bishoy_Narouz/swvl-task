import validateString from './Base/string'
import validateEmail from './Base/email'
import validateRole from './Base/role'
import validatephone from './Base/phone'
import validateObjectId from './Base/objectId'
import validateObjectIds from './Base/objectIds'
import validatePassword from './Custom/validatePassword'
import spacesNotAllowed from './Custom/notAllowedSpaces'
import validateToken from './Custom/validateToken'

module.exports = {
    validateString: validateString,
    validateEmail: validateEmail,
    validateRole: validateRole,
    validatephone: validatephone,
    validateObjectId: validateObjectId,
    validateObjectIds: validateObjectIds,
    validatePassword: validatePassword,
    spacesNotAllowed: spacesNotAllowed,
    validateToken: validateToken
}