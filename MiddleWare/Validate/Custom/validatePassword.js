import validator from 'validator'
import errors from '../../Errors/index'

module.exports = function (password) {
    if (!password) {
        throw new errors.Missing({ fieldName: 'Password' })
    }
    if (typeof password !== 'string') {
        throw new errors.Invalid({ message: 'Password must be string' })
    }
    if (validator.contains(password, ' ')) {
        throw new errors.Invalid({ message: `Spaces is not Allowed in Password` })
    }
    if (password.length < 10) {
        throw new errors.Invalid({ message: `Password minimum length must be greater than 10 digits` })
    }
    if (password.length > 25) {
        throw new errors.Invalid({ message: `Password maximum length must be less than 25 digits` })
    }
    if (/[A-Z]/.test(password) === false) {
        throw new errors.Invalid({ message: `Password must contain at least one capital letter` })
    }
    if (/[!@#$%^&*(),.?":{}|<>]/.test(password) === false) {
        throw new errors.Invalid({ message: `Password must contain special character` })
    }
    else {
        return password
    }
}
