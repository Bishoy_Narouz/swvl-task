import randomstring from 'randomstring'
// import validator from 'validator'
import db from '../../../Database/Models/index'
import Success from '../../../MiddleWare/Errors/Models/SuccessResponse'
import errorMessageEnum from '../../../Enums/ErrorMessageEnum'
import errors from '../../../MiddleWare/Errors/index'

module.exports = async function (req, res, next) {
    try {
        if (!req.body.promoCodeNumber) {
            throw new errors.Missing({ fieldName: 'Promo Code Number' })
        }
        else if (typeof req.body.promoCodeNumber !== 'number') {
            throw new errors.Invalid({ message: errorMessageEnum.PROMO_CODE_NUMBER_MUST_BE_INTEGER })
        }
        else if (req.body.promoCodeNumber <= 0) {
            throw new errors.Invalid({ message: errorMessageEnum.PROMO_CODE_NUMBER_MUST_BE_GREATER_THAN_ZERO })
        }
        else if (req.body.promoCodeNumber > 50) {
            throw new errors.Invalid({ message: errorMessageEnum.PROMO_CODE_NUMBER_MUST_BE_LESS_THAN_FIFTY })
        }
        else {
            let promoCodesArray = []
            for (let i = 1; i <= req.body.promoCodeNumber; i++) {
                let pc = randomstring.generate(6)
                promoCodesArray.push({ promoCode: pc })
            }

            let result = await db.PromoCode.create(promoCodesArray)
            res.send(new Success(promoCodesArray))
        }
    } catch (error) {
        console.log(error)
        res.send(error)
    }
}