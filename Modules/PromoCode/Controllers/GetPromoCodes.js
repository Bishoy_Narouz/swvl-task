import db from '../../../Database/Models/index'
import Success from '../../../MiddleWare/Errors/Models/SuccessResponse'

module.exports = async function (req, res, next) {
    try {
        let promoCodes = await db.PromoCode.find().select({ _id: 0, __v: 0 }).lean()
        promoCodes = promoCodes.map(row => {
            if (row.user) {
                row.isUsed = true
            } else {
                row.isUsed = false
            }
            return row
        })
        res.send(new Success(promoCodes))
    } catch (error) {
        res.send(error)
    }
}