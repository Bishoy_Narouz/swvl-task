import createPromoCodes from './CreatePromoCodes'
import getPromoCodes from './GetPromoCodes'

module.exports = {
    createPromoCodes: createPromoCodes,
    getPromoCodes: getPromoCodes
}