import express from 'express'
import promoCodeControllers from './Controllers/index'
import rolesEnum from '../../Enums/RolesEnum'
import validateToken from '../../MiddleWare/Validate/Custom/validateToken'

const router = express.Router()

router.post('/createPromoCodes', validateToken([rolesEnum.ADMIN]), promoCodeControllers.createPromoCodes)

router.get('/getPromoCodes', validateToken([rolesEnum.ADMIN]), promoCodeControllers.getPromoCodes)

module.exports = router