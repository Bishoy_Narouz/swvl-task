import db from '../../../Database/Models/index'
import Success from '../../../MiddleWare/Errors/Models/SuccessResponse'
import errors from '../../../MiddleWare/Errors/index'
import validate from '../../../MiddleWare/Validate/index'
import errorMessageEnum from '../../../Enums/ErrorMessageEnum'
import MailService from '../../../MiddleWare/Services/Mail'

module.exports = async function (req, res, next) {
    try {
        validate.validateString({ string: req.body.textEn, fieldName: 'Text En', optional: false, language: 'en' })
        validate.validateString({ string: req.body.textAr, fieldName: 'Text Ar', optional: false, language: 'ar' })
        validate.validateObjectId({ objectId: req.body.userId, fieldName: 'User Id' })

        const user = await db.User.findOne({ _id: req.body.userId })
        if (!user) {
            throw new errors.NotFound({ message: errorMessageEnum.USER_IS_NOT_FOUND })
        }

        MailService.SendMessage(user.username, user.email, (user.lang === 'En') ? req.body.textEn : req.body.textAr)

        res.send(new Success())
    } catch (error) {
        res.send(error)
    }
}
