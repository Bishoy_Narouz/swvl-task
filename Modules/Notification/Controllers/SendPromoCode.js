import db from '../../../Database/Models/index'
import Success from '../../../MiddleWare/Errors/Models/SuccessResponse'
import errors from '../../../MiddleWare/Errors/index'
import validate from '../../../MiddleWare/Validate/index'
import errorMessageEnum from '../../../Enums/ErrorMessageEnum'
import SMSService from '../../../MiddleWare/Services/SMS'
import MailService from '../../../MiddleWare/Services/Mail'

module.exports = async function (req, res, next) {
    try {
        validate.validateString({ string: req.body.promoCodeMessage, fieldName: 'Promo Code Message', optional: false })
        validate.validateString({ string: req.body.promoCode, fieldName: 'Promo Code', optional: false })
        validate.validateObjectId({ objectId: req.body.userId, fieldName: 'User Id' })

        const isPromoCodeExists = await db.PromoCode.findOne({ promoCode: req.body.promoCode })
        if (!isPromoCodeExists) {
            throw new errors.NotFound({ message: errorMessageEnum.PROMO_CODE_IS_NOT_FOUND })
        }

        if (isPromoCodeExists.user) {
            throw new errors.Invalid({ message: errorMessageEnum.PROMO_CODE_ALREADY_TAKEN })
        }

        const user = await db.User.findOne({ _id: req.body.userId })
        if (!user) {
            throw new errors.NotFound({ message: errorMessageEnum.USER_IS_NOT_FOUND })
        }

        MailService.SendPromoCode(user.username, user.email, req.body.promoCode)
        SMSService.SendPromoCode(user.mobile, req.body.promoCodeMessage, req.body.promoCode)

        await db.PromoCode.updateOne({ _id: isPromoCodeExists._id }, { $set: { user: user._id } })

        res.send(new Success())
    } catch (error) {
        res.send(error)
    }
}
