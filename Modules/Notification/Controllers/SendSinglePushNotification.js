import Success from '../../../MiddleWare/Errors/Models/SuccessResponse'
import db from '../../../Database/Models/index'
import validate from '../../../MiddleWare/Validate/index'
import pushNotificationService from '../../../MiddleWare/Services/PushNotification'
import errors from '../../../MiddleWare/Errors/index'
import errorMessageEnum from '../../../Enums/ErrorMessageEnum'

module.exports = async function (req, res, next) {
    try {
        validate.validateString({ string: req.body.textEn, fieldName: 'Text En', optional: false, language: 'en' })
        validate.validateString({ string: req.body.textAr, fieldName: 'Text Ar', optional: false, language: 'ar' })
        validate.validateObjectId({ objectId: req.body.userId, fieldName: 'User Id' })

        const user = await db.User.findOne({ _id: req.body.userId })
        if (!user) {
            throw new errors.NotFound({ message: errorMessageEnum.USER_IS_NOT_FOUND })
        } else if (!user.deviceToken) {
            throw new errors.NotFound({ message: errorMessageEnum.USER_HAS_NO_DEVICE_TOKEN })
        } else {
            await pushNotificationService((user.lang === 'En') ? req.body.textEn : req.body.textAr, user.deviceToken)
        }

        res.send(new Success())
    } catch (error) {
        console.log(error)
        res.send(error)
    }
}