import Success from '../../../MiddleWare/Errors/Models/SuccessResponse'
import db from '../../../Database/Models/index'
import validate from '../../../MiddleWare/Validate/index'
import pushNotificationService from '../../../MiddleWare/Services/PushNotification'
import errors from '../../../MiddleWare/Errors/index'

module.exports = async function (req, res, next) {
    try {
        validate.validateString({ string: req.body.textEn, fieldName: 'Text En', optional: false, language: 'en' })
        validate.validateString({ string: req.body.textAr, fieldName: 'Text Ar', optional: false, language: 'ar' })
        validate.validateObjectIds({ array: req.body.userIds, fieldName: 'User Ids' })

        if (req.body.userIds.length > 10) {
            throw new errors.Invalid({ message: errorMessageEnum.MAX_NUMBER_OF_USER_IDS_IS_10 })
        }

        for (let id of req.body.userIds) {
            let user = await db.User.findOne({ _id: id })
            if (user && user.deviceToken) {
                await pushNotificationService((user.lang === 'En') ? req.body.textEn : req.body.textAr, user.deviceToken)
            }
        }

        res.send(new Success())
    } catch (error) {
        console.log(error)
        res.send(error)
    }
}