import sendSingleSMS from './SendSingleSMS'
import sendMultipleSMS from './SendMultipleSMS'
import sendPromoCode from './SendPromoCode'
import sendSinglePushNotification from './SendSinglePushNotification'
import sendMultiplePushNotifications from './SendMultiplePushNotifications'
import sendMail from './SendMail'

module.exports = {
    sendPromoCode: sendPromoCode,
    sendSingleSMS: sendSingleSMS,
    sendMultipleSMS: sendMultipleSMS,
    sendSinglePushNotification: sendSinglePushNotification,
    sendMultiplePushNotifications: sendMultiplePushNotifications,
    sendMail: sendMail
}