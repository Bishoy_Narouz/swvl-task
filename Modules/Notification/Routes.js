import express from 'express'
import notificationControllers from './Controllers/index'
import validateToken from '../../MiddleWare/Validate/Custom/validateToken'
import rolesEnum from '../../Enums/RolesEnum'

const router = express.Router()

router.post('/sendPromoCode', validateToken([rolesEnum.ADMIN]), notificationControllers.sendPromoCode)

router.post('/sendSingleSMS', validateToken([rolesEnum.ADMIN]), notificationControllers.sendSingleSMS)

router.post('/sendMultipleSMS', validateToken([rolesEnum.ADMIN]), notificationControllers.sendMultipleSMS)

router.post('/sendSinglePushNotification', validateToken([rolesEnum.ADMIN]), notificationControllers.sendSinglePushNotification)

router.post('/sendMultiplePushNotifications', validateToken([rolesEnum.ADMIN]), notificationControllers.sendMultiplePushNotifications)

router.post('/sendEmail', validateToken([rolesEnum.ADMIN]), notificationControllers.sendMail)

module.exports = router