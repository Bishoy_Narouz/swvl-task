import validate from '../../../MiddleWare/Validate/index'
import jwt from 'jsonwebtoken'
import config from '../../../Config'
import Success from '../../../MiddleWare/Errors/Models/SuccessResponse'
import errors from '../../../MiddleWare/Errors/index'

module.exports = async function (req, res, next) {
    try {
        validate.validateString({ string: req.body.refreshToken, fieldName: 'Refresh Token', optional: false })

        jwt.verify(req.body.refreshToken, config.RefreshToken.Key, (err, decoded) => {
            if (err) {
                if (err.name === 'TokenExpiredError') {
                    res.send(new errors.ExpiredTokenError())
                } else {
                    res.send(new errors.InvalidTokenError())
                }
            }
            const token = jwt.sign({ userId: decoded.userId, role: decoded.role }, config.Token.Key, { expiresIn: config.Token.LifeTime })
            const refreshToken = jwt.sign({ userId: decoded.userId, role: decoded.role }, config.RefreshToken.Key, { expiresIn: config.RefreshToken.LifeTime })
            res.send(new Success({ Tokens: { Token: token, RefreshToken: refreshToken } }))
        })
    } catch (error) {
        res.send(error)
    }
}