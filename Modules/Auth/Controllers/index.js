import login from './Login'
import signup from './Signup'
import refreshToken from './RefreshToken'

module.exports = {
    login: login,
    signup: signup,
    refreshToken: refreshToken
}