import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import config from '../../../Config'
import Success from '../../../MiddleWare/Errors/Models/SuccessResponse'
import errors from '../../../MiddleWare/Errors/index'
import db from '../../../Database/Models/index'
import validate from '../../../MiddleWare/Validate/index'
import errorMessageEnum from '../../../Enums/ErrorMessageEnum'
import rolesEnum from '../../../Enums/RolesEnum'

module.exports = async function (req, res, next) {
    try {
        const credential = {
            identity: (req.body.identity) ? req.body.identity.trim() : null,
            password: (req.body.password) ? req.body.password.trim() : null
        }

        validate.validateString({ string: credential.identity, fieldName: 'Identity', optional: false })
        validate.validateString({ string: credential.password, fieldName: 'Password', optional: false })

        const user = await db.User.findOne(
            {
                $and: [
                    { role: rolesEnum.ADMIN },
                    {
                        $or: [
                            { username: credential.identity },
                            { email: credential.identity }
                        ]
                    }
                ]
            }
        )

        if (!user || !bcrypt.compareSync(credential.password, user.password)) {
            throw new errors.Invalid({ message: errorMessageEnum.INVALID_CREDENTIAL })
        }

        const token = jwt.sign({ userId: user._id, role: user.role }, config.Token.Key, { expiresIn: config.Token.LifeTime })
        const refreshToken = jwt.sign({ userId: user._id, role: user.role }, config.RefreshToken.Key, { expiresIn: config.RefreshToken.LifeTime })

        let result = {
            currentUser: {
                username: user.username,
                email: user.email,
                mobile: user.mobile,
                role: user.role
            },
            tokens: {
                token: token,
                refreshToken: refreshToken
            }
        }

        res.send(new Success(result))
    } catch (error) {
        res.send(error)
    }
}
