import bcrypt from 'bcrypt'
import config from '../../../Config'
import langEnum from '../../../Enums/LangEnum'
import Success from '../../../MiddleWare/Errors/Models/SuccessResponse'
import errors from '../../../MiddleWare/Errors/index'
import db from '../../../Database/Models/index'
import validate from '../../../MiddleWare/Validate/index'
import errorMessageEnum from '../../../Enums/ErrorMessageEnum'

module.exports = async function (req, res, next) {
    try {
        let user = new db.User({
            username: (req.body.username) ? req.body.username.trim() : null,
            email: (req.body.email) ? req.body.email.trim() : null,
            password: (req.body.password) ? req.body.password.trim() : null,
            mobile: (req.body.mobile) ? req.body.mobile : null,
            deviceToken: (req.body.deviceToken) ? req.body.deviceToken.trim() : null,
            lang: (req.body.lang) ? req.body.lang.trim() : null
        })

        validate.validateString({ string: user.username, fieldName: 'Username', optional: false, min: 3, max: 20 })
        validate.spacesNotAllowed({ string: user.username, fieldName: 'Username' })
        validate.validateEmail({ email: user.email, optional: false })
        validate.validatephone({ phone: user.mobile, optional: false })
        validate.validatePassword(user.password)
        validate.validateString({ string: user.deviceToken, fieldName: 'Device Token', optional: true })
        validate.validateString({ string: user.lang, fieldName: 'Language', optional: true })
        validate.validatePassword(user.password)

        if (user.lang && ![langEnum.ENGLISH, langEnum.ARABIC].includes(user.lang)) {
            throw new errors.Invalid({ message: errorMessageEnum.INVALID_LANGUAGE })
        }

        const salt = bcrypt.genSaltSync(config.SaltRounds)
        user.password = bcrypt.hashSync(user.password, salt)

        let result = await user.save()

        res.send(new Success(result))
    } catch (error) {
        res.send(error)
    }

}