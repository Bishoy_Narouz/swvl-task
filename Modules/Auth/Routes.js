import express from 'express'
import authControllers from './Controllers/index'

const router = express.Router()


router.post('/login', authControllers.login)

router.post('/signup', authControllers.signup)

router.post('/refreshToken', authControllers.refreshToken)

module.exports = router;