import express from 'express'
import userControllers from './Controllers/index'
import rolesEnum from '../../Enums/RolesEnum'
import validateToken from '../../MiddleWare/Validate/Custom/validateToken'

const router = express.Router()

router.get('/getUsers', validateToken([rolesEnum.ADMIN]), userControllers.getUsers)

module.exports = router