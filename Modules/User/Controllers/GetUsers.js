import db from '../../../Database/Models/index'
import Success from '../../../MiddleWare/Errors/Models/SuccessResponse'
import rolesEnum from '../../../Enums/RolesEnum'

module.exports = async function (req, res, next) {
    try {
        const users = await db.User.find({ role: rolesEnum.USER }).select('_id username email mobile deviceToken lang')
        res.send(new Success(users))
    } catch (error) {
        res.send(error)
    }
}