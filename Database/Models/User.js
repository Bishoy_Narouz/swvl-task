import rolesEnum from '../../Enums/RolesEnum'
import langEnum from '../../Enums/LangEnum'
import errors from '../../MiddleWare/Errors/index'
import errorMessageEnum from '../../Enums/ErrorMessageEnum'

module.exports = (mongoose) => {
    const userSchema = new mongoose.Schema({
        username: {
            type: String,
            required: true,
            unique: true,
            trim: true,
            min: 6,
            max: 12
        },
        email: {
            type: String,
            required: true,
            unique: true,
            trim: true
        },
        mobile: {
            type: String,
            required: true,
            unique: true,
            trim: true
        },
        password: {
            type: String,
            required: true,
            trim: true,
            min: 10,
            max: 20
        },
        role: {
            type: String,
            required: true,
            default: rolesEnum.USER
        },
        deviceToken: {
            type: String,
            required: false
        },
        lang: {
            type: String,
            required: true,
            default: langEnum.ENGLISH
        }
    }, { timestamps: { createdAt: true, updatedAt: false, versionKey: false } })

    userSchema.index({ _id: 1, username: 1, email: 1 })

    userSchema.pre('save', async function (next) {
        var self = this
        const isUserExist = await User.findOne({ $and: [{ username: self.username }, { email: self.email }] })
        if (isUserExist) {
            next(new errors.DuplicateError({ message: errorMessageEnum.USER_IS_DUPLICATED }))
        } else {
            next()
        }
    })

    const User = mongoose.model('User', userSchema)

    return User
}