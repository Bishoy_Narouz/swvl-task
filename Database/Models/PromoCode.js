module.exports = (mongoose) => {
    const promoCodeSchema = new mongoose.Schema({
        promoCode: {
            type: String,
            required: true,
            unique: true
        },
        user: {
            type: mongoose.Schema.Types.ObjectId, ref: 'User',
            required: false
        }
    }, { versionKey: false })

    promoCodeSchema.index({ promoCode: 1 })

    const PromoCode = mongoose.model('PromoCode', promoCodeSchema)

    return PromoCode
}