import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import config from './Config'
import dbConfig from './Database/DBConfig'
import swaggerUi from 'swagger-ui-express'
import YAML from 'yamljs'
import AuthRoutes from './Modules/Auth/Routes'
import UserRoutes from './Modules/User/Routes'
import PromoCodeRoutes from './Modules/PromoCode/Routes'
import NotificationRoutes from './Modules/Notification/Routes'

const app = express()
app.use(bodyParser.json({ limit: config.RequestSizeLimit }))
app.use(bodyParser.urlencoded({ limit: config.RequestSizeLimit, extended: true }))
app.use(cors(config.corsOptions))

app.use('/api/auth', AuthRoutes)
app.use('/api/user', UserRoutes)
app.use('/api/promoCode', PromoCodeRoutes)
app.use('/api/notification', NotificationRoutes)

const swaggerDocument = YAML.load('./swagger.yaml')
app.use('/swvl-backend-apis', swaggerUi.serve, swaggerUi.setup(swaggerDocument, config.swaggerOptions))

app.listen(config.BackendPort, () => {
    console.log(`the app listening on port ${config.BackendPort}`)
    console.log(`Swagger Apis URL :  ${config.SwaggerUrl}`)
})