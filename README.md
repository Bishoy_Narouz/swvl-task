# Swvl Backend Challenge
The main feature of this application to communicate with Swvl customers via notifications
## Technologies
- NodeJS (ExpressJS Framework)
- Docker containers 
- MongoDB (atlas cloud) 
- SendGrid (Sending emails)
- One-Signal (Push Notification server)
- Nexmo (SMS Gateway provider)
## Installation
### Docker method 
User [Docker](https://www.docker.com)  to install the docker package
- Ubuntu 
```bash
 apt install docker 
``` 
- Centos/Redhat 
```bash
yum install docker 
```
#### Pull the docker repo 
The below command will allow you to pull the public repo for the project 
```bash
docker pull bishoynarouz/swvl-task
```
#### Run Docker container 
The below command will allow you to run the docker container to access the project 
```bash
docker run --name swvl-task-bishoy -p 8080:8080 bishoynarouz/swvl-task
```
Once you run the project you can see the below output 
```bash
swvl-task@1.0.0 start /home/node/app
nodemon --exec babel-node server.js
[nodemon] 2.0.2
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `babel-node server.js`
the app listening on port 8080
Swagger Apis URL :  http://localhost:8080/swvl-backend-apis
```
Now you can access the project using the following [URL](http://localhost:8080/swvl-backend-apis) 
### NPM method
- Ubuntu 
```bash
 apt install npm node git 
``` 
- Centos/Redhat 
```bash
yum install npm node git 
```
#### Pull the repo 
The below command will allow you to pull the public repo for the project 
```bash
git clone https://Bishoy_Narouz@bitbucket.org/Bishoy_Narouz/swvl-task.git 
```
#### Run node project  
The below command will install the npm packages for the project  
```bash
npm install
```
The below command will run the project   
```bash
npm start
```
Once you run the project you can see the below output 
```bash
swvl-task@1.0.0 start /home/node/app
nodemon --exec babel-node server.js
[nodemon] 2.0.2
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `babel-node server.js`
the app listening on port 8080
Swagger Apis URL :  http://localhost:8080/swvl-backend-apis
```
Now you can access the project using the following [URL](http://localhost:8080/swvl-backend-apis) 
# Thank you! 