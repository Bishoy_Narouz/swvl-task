const env = typeof process.env.NODE_ENV === 'undefined' ? 'local' : process.env.NODE_ENV
const config = require(`./${env}`)

module.exports = {
    BackendPort: config.BackendPort,
    DBUrl: config.DBUrl,
    swaggerOptions: config.swaggerOptions,
    SwaggerUrl: config.SwaggerUrl,
    SaltRounds: config.SaltRounds,
    corsOptions: config.corsOptions,
    RequestSizeLimit: config.RequestSizeLimit,
    Token: config.Token,
    RefreshToken: config.RefreshToken,
    SendGridKey: config.SendGridKey,
    Nexmo: config.Nexmo,
    OneSignal: config.OneSignal
}