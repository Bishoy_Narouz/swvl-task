module.exports = {
    BackendPort: 8080,
    DBUrl: 'mongodb+srv://swvl-user:swvl-12345678@swvl-task-nbibl.mongodb.net/swvl-db?retryWrites=true&w=majority',
    swaggerOptions: {
        explorer: true
    },
    SwaggerUrl : 'http://localhost:8080/swvl-backend-apis',
    SaltRounds: 10,
    corsOptions: {
        origin: '*',
        optionsSuccessStatus: 200
    },
    RequestSizeLimit: '10mb',
    Token: {
        Key: '0AQ9E8vTH(~3^AsMttbXR4B^TQg0r)BEH8DgeqRCbUJLgkREUtlnv@wmQU9WZPiA+qJW4mHHr>MmyEUSCt5UC>=Ft:7CzoSYRkQ',
        LifeTime: '1h'
    },
    RefreshToken: {
        Key: 'qRCbUJLgkREUtlnv@w0AQ9E8vTHBEH8DgemQU(~3^AsMttbXR4B^TQg0r)9WZPiA+qJW4zoSYRkQmHHr>MmyEUSCt5UC>=Ft:7C',
        LifeTime: '1d'
    },
    SendGridKey: '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',
    OneSignal: {
        UserAuthKey: 'YmVhZTI5NzAtMGI2My00ZWFjLWExNjYtYWUyNDM5NTAwZTZj',
        AppAuthKey: 'MmZhMWFlZTMtYzYzNS00MjE5LWJlY2EtNjFjOTJjNDM1ZDBl',
        AppId: '8310e08a-9e74-406a-b83a-0b5417d1c88c'
    },
    Nexmo: {
        ApiKey: '8502189b',
        ApiSecret: 'YtToF6iN4jVLJ9Qe'
    }
}