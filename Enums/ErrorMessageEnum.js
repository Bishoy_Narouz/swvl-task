const ErrorMessageEnum =
{
    UNAUTHORIZED: 'UNAUTHORIZED',
    UNAUTHENTICATED: 'UNAUTHENTICATED',
    INVALID_TOKEN: 'INVALID_TOKEN',
    TOKEN_EXPIRED_ERROR: 'TOKEN_EXPIRED_ERROR',
    REQUIRED: 'REQUIRED',
    BAD_REQUEST: 'BAD_REQUEST',
    INVALID_ROLE: 'User Role is not valid',
    USER_IS_DUPLICATED: 'Username or Email Already Exists',
    USER_IS_NOT_FOUND: 'User is not found',
    USER_IS_BLOCKED: 'User is blocked',
    CURRENT_PASSWORD_INCORRECT: 'Current Password is not correct',
    INVALID_RESET_PASSWORD_CODE: 'Password Code is not valid',
    INVALID_CREDENTIAL: 'Invalid Credential',
    EMAIL_IS_NOT_FOUND: 'Email is not found',
    INVALID_USER_ROLE: 'Invalid User Role',
    INVALID_LANGUAGE: 'Invalid Language',
    PROMO_CODE_MUST_BE_FIVE_CHARACTERS: 'Promo Code length must be 5 characters',
    PROMO_CODE_NUMBER_MUST_BE_INTEGER: 'Promo Code Number must be integer',
    PROMO_CODE_NUMBER_MUST_BE_GREATER_THAN_ZERO: 'Promo Code Number must be greater than zero',
    PROMO_CODE_NUMBER_MUST_BE_LESS_THAN_FIFTY: 'Promo Code Number must be less than 50',
    PROMO_CODE_IS_NOT_FOUND: 'Promo Code is not found',
    PROMO_CODE_ALREADY_TAKEN: 'Promo Code Already Taken By Another User',
    MAX_NUMBER_OF_USER_IDS_IS_10: 'the max number of user ids is 10',
    USER_HAS_NO_DEVICE_TOKEN: 'User does not has device token'
}

module.exports = ErrorMessageEnum